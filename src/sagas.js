import { takeLatest, put, call, fork, take, cancel } from 'redux-saga/effects';
import {createProjectSuccess, syncProjects} from "./store/actions/projectActions";
import reduxSagaFirebase from './config/firebaseConfig'
import {loginFailure, loginSuccess} from "./store/actions/authActions";

export function* createProjectSaga({payload}) {
    //create Project API Call
    const doc = yield call(
        reduxSagaFirebase.firestore.addDocument,
        'projects',
        payload
    );
    console.log('Creating Project');
    console.log(doc);
    yield put(createProjectSuccess(payload));
}

export function* syncFirebaseProjectsSaga() {
    // Start the sync saga
    let task = yield fork(
        reduxSagaFirebase.firestore.syncCollection, 'projects', {
            successActionCreator: syncProjects,
            transform: projectTransformer
        }
    );

    // Wait for the logout action, then stop sync
    yield take('LOGOUT');
    yield cancel(task);
}

export function* loginSaga({credentials}) {
    try {
        // const {email, password} = credentials;
        // console.log(credentials,'LOGGING IN++++++++++');
        const data = yield call(
            reduxSagaFirebase.auth.signInWithEmailAndPassword,
            credentials.email,
            credentials.password);
        yield put(loginSuccess(data));
        console.log('LOGGING DONE=========')
    }
    catch(error) {
        console.log('LOGGING ERROR')
        yield put(loginFailure(error));
    }
}

export default function* sagas() {
    fork(syncFirebaseProjectsSaga);
    yield takeLatest('CREATE_PROJECT_REQUEST', createProjectSaga);
    yield takeLatest('CREATE_PROJECT_SUCCESS', syncFirebaseProjectsSaga)
    yield takeLatest('GET_PROJECTS', syncFirebaseProjectsSaga)
    yield takeLatest('LOGIN_REQUEST', loginSaga)
}

const projectTransformer = projects => {
    const result = []
    projects.forEach(doc =>
        result.push({
            id: doc.id,
            ...doc.data(),
        }),
    )
    return result
}