export const loginRequest = (credentials) => ({
    type: 'LOGIN_REQUEST',
    credentials
});

export const loginSuccess = (data) => ({
    type: 'LOGIN_SUCCESS',
    data
});

export const loginFailure = (error) => ({
    type: 'LOGIN_ERROR',
    error
});