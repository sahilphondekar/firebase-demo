import React from "react";

const ProjectSummary = (props) => {
    // console.log(props);
    return (
        <div className="card-body border m-2">
            <h5 className="card-title">{props.project.title}</h5>
            <p className="card-text">{props.project.content}</p>
            <p className="card-text text-secondary">5th April, 5:00pm</p>
        </div>
    );
};

export default ProjectSummary;