import React, {Component} from "react";
import { createProjectRequest } from '../../store/actions/projectActions'
import {connect} from "react-redux";

class CreateProject extends Component{
    state = {
        title: "",
        content: ""
    };

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.createProject(this.state);
        console.log(this.state);
    };

    render() {
        return (
            <div className="container">
                <h5 className="text-center">Create new project</h5>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input type="text" className="form-control" id="title" onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="content">Project Content</label>
                        <textarea className="form-control" id="content" onChange={this.handleChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Create</button>
                </form>
            </div>
        );
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        createProject: (data) => dispatch(createProjectRequest(data))
    }
}

export default connect(null, mapDispatchToProps)(CreateProject);