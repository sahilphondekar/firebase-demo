import React from "react"
import {NavLink} from "react-router-dom";

const SignedInLinks = () => {
    return (
        <div className="navbar-nav mx-5">
            <NavLink to='/create' className="nav-link">New Project</NavLink>
            <NavLink to='/' className="nav-item nav-link">Log Out</NavLink>
            <NavLink to='/' className="nav-item nav-link bg-light text-secondary rounded-circle">NN</NavLink>
        </div>
    )
};

export default SignedInLinks;