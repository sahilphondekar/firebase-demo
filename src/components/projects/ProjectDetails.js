import React from "react";

const ProjectDetails = (props) => {
    const id = props.match.params.id;
    console.log(props);
    return (
        <div className="container">
            <div className="card-body">
                <h5 className="card-title">Project title - {id}</h5>
                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Consequatur cum doloribus eaque modi neque, quisquam sapiente! Aperiam cumque,
                    dicta, dolores eaque enim esse ipsum iste labore, magnam porro ratione voluptatibus.</p>
                <p className="card-text"><small className="text-muted">Posted by Sahil</small></p>
                <p className="card-text"><small className="text-muted">5th April, 5:00pm</small></p>
            </div>
        </div>
    );
};

// const mapStateToProps = (state, ownProps) => {
//     const id = ownProps.match.params.id;
//     const projects = state.project.projects;
//     const project = projects ? projects[id] : null;
//     return {
//         project: project
//     }
// }

export default ProjectDetails;