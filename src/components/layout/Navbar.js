import React from "react"
import {Link} from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link to='/' className="navbar-brand offset-2">Navbar</Link>
            <SignedInLinks/>
            <SignedOutLinks/>
        </nav>
    );
};

export default Navbar;