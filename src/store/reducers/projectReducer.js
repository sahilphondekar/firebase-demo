const initState = {
    projects: []
};

const projectReducer = (state = initState, action) => {
    switch(action.type) {
        // case 'CREATE_PROJECT_SUCCESS':
        //     console.log('Project Created');
        //     return {
        //         ...state,
        //         projects: action.projects
        //     }
        case 'SYNC_PROJECTS':
            console.log('project sync started');
            console.log(action.projects);
            console.log('----------------------');
            return {
                ...state,
                projects: action.projects
            }
        default:
            return state;
    }
};

export default projectReducer;