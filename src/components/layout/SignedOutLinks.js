import React from "react"
import {NavLink} from "react-router-dom";

const SignedOutLinks = () => {
    return (
        <div className="navbar-nav mx-5">
            <NavLink to='/signup' className="nav-link">Signup</NavLink>
            <NavLink to='/signin' className="nav-item nav-link">Login</NavLink>
        </div>
    )
};

export default SignedOutLinks;