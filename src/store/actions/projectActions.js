export const createProjectRequest = (payload) => ({
    type: 'CREATE_PROJECT_REQUEST',
    payload
});

export const createProjectSuccess = (data) => ({
    type: 'CREATE_PROJECT_SUCCESS',
    data
});

export const syncProjects = (projects) => ({
    type: 'SYNC_PROJECTS',
    projects
});

export const getProjects = () => ({
    type: 'GET_PROJECTS',
});