import React, {Component} from "react"
import Notifications from "./Notifications";
import ProjectList from "../projects/ProjectList";
import {connect} from "react-redux";
import {getProjects} from "../../store/actions/projectActions";

class Dashboard extends Component{

    componentDidMount() {
        console.log('Dashboard******************')
        this.props.getProjects();
        console.log(this.props.projects, '* ************PROJECTS');
    }

    render() {
        // console.log(this.props);
        const projects = this.props;
        return (
            <div className='container'>
                <div className="row">
                    <div className="offset-1 col-4">
                        <ProjectList projects={projects}/>
                    </div>
                    <div className="offset-2 col-4">
                        <Notifications/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    projects: state.project.projects
});

const mapDispatchToProps = (dispatch) => {
    return {
        getProjects: () => dispatch(getProjects())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);