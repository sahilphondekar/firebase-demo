import React, {Component} from "react";
import {connect} from "react-redux";
import {loginRequest} from "../../store/actions/authActions";

class SignIn extends Component{
    state = {
        email: "",
        password: ""
    };

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.login(this.state);
        console.log(this.state);
    };

    render() {
        return (
            <div className="container">
                <h5 className="text-center">Sign In</h5>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email" onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" onChange={this.handleChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Sign In</button>
                </form>
            </div>
        );
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (credentials) => dispatch(loginRequest(credentials))
    }
}

export default connect(null, mapDispatchToProps)(SignIn);