import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import ReduxSagaFirebase from 'redux-saga-firebase'

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBT8SxonihgAPq6-0KEWkfMMrsZWG-0VOo",
    authDomain: "reactfirebasedemo-c603f.firebaseapp.com",
    databaseURL: "https://reactfirebasedemo-c603f.firebaseio.com",
    projectId: "reactfirebasedemo-c603f",
    storageBucket: "reactfirebasedemo-c603f.appspot.com",
    messagingSenderId: "522275832094",
    appId: "1:522275832094:web:b9f712781dddd64c2a490f",
    measurementId: "G-9DHGKW52VS"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

const reduxSagaFirebase = new ReduxSagaFirebase(firebaseApp)

// firebase.firestore().settings({ timestampsInSnapshots: true })

export default reduxSagaFirebase;